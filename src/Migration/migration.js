import React from "react";
import data from "./rise-47468-export.json";
import { firestore } from "./../Firebase/utils";
import MigrateBtn from "./migrateBtn";
import firebase from "firebase";
const Migration = () => {
  const migrateUsers = () => {
    //Migrating Users to firestore
    // const userRef = firestore.collection("profiles");
    // Object.keys(data.user).map(async (key) => {
    //   await userRef.doc(key).set(data.user[key]);
    // });
    // alert("users Migrated to firestore");
  };
  const migrateFeeds = () => {
    var postRef = firestore.collection("posts");
    var feedRef = firestore.collection("feeds");
    //Migrating Feeds-Posts
    // Object.keys(data.feeds).map((uid) => {
    //   Object.keys(data.feeds[uid]).map((postID) => {
    //     const { content, created, edited } = data.feeds[uid][postID];

    //     if (created && content) {
    //       postRef.doc(postID).set({
    //         author: uid,
    //         content: content ? content : "",
    //         created: created ? new Date(created) : "",
    //         edited: edited ? new Date(edited) : "",
    //       });

    //       feedRef.doc(uid).set(
    //         {
    //           createdBy: uid,
    //           [postID]: created ? new Date(created) : "",
    //         },
    //         { merge: true }
    //       );
    //     }
    //   });
    // });
  };
  const migrateComments = () => {
    var cmnt_threadRef = firestore.collection("comment_threads");
    var cmnt_ref = firestore.collection("comments");
    //Migrating Comments to firstore
    // Object.keys(data.feeds).map((uid) => {
    //   Object.keys(data.feeds[uid]).map((postId) => {
    //     if (data.feeds[uid][postId].comments) {
    //       Object.keys(data.feeds[uid][postId].comments).map((cmntID) => {
    //         const { content, edited, created } = data.feeds[uid][postId].comments[cmntID];
    //         console.log({
    //           postId: postId,
    //           commentID: cmntID,
    //         });
    //         cmnt_threadRef.doc(postId).set(
    //           {
    //             [cmntID]: created,
    //           },
    //           { merge: true }
    //         );
    //         cmnt_ref.doc(cmntID).set(
    //           {
    //             author: uid,
    //             content: content,
    //             created: created ? created : "",
    //             edited: edited ? edited : "",
    //           },
    //           { merge: true }
    //         );
    //       });
    //     }
    //   });
    // });
  };
  const migrateLikes = () => {
    var likeRef = firestore.collection("like_threads");
    //Migrate likes of users
    // Object.keys(data.feeds).map((uid) => {
    //   Object.keys(data.feeds[uid]).map((postId) => {
    //     if (data.feeds[uid][postId].likes) {
    //       Object.keys(data.feeds[uid][postId].likes).map((likeId) => {
    //         likeRef.doc(postId).set(
    //           {
    //             [data.feeds[uid][postId].likes[likeId]]: new Date(),
    //           },
    //           {
    //             merge: true,
    //           }
    //         );
    //       });
    //     }
    //   });
    // });
  };
  const migrateMessages = async () => {
    //migrating message keys
    // var messageKeys = {};
    // var chat_thread = firestore.collection("chat_threads");
    // await firestore
    //   .collection("messages")
    //   .get()
    //   .then((snap) => {
    //     snap.forEach((data) => {
    //       messageKeys[data.id] = data.data();
    //     });
    //   });
    // Object.keys(messageKeys).map(async (messageKeyID) => {
    //   const { users } = messageKeys[messageKeyID];
    //   await chat_thread.doc(users[0]).set(
    //     {
    //       [messageKeys[messageKeyID].key]: {
    //         chatWith: users[1],
    //         created: new Date(),
    //       },
    //     },
    //     { merge: true }
    //   );
    //   await chat_thread.doc(users[1]).set(
    //     {
    //       [messageKeys[messageKeyID].key]: {
    //         chatWith: users[0],
    //         created: new Date(),
    //       },
    //     },
    //     { merge: true }
    //   );
    // });
    //Migrating Message
    // var chatRef = firestore.collection("chats");
    // const { messages } = data;
    // Object.keys(messages).map((messageID) => {
    //   messages[messageID].map(async (message) => {
    //     const {
    //       sender,
    //       date,
    //       read,
    //       received,
    //       message: messageContent,
    //       deletedBySenderID,
    //     } = message;
    //     let messageData = {};
    //     if (deletedBySenderID) {
    //       messageData = {
    //         author: sender,
    //         created: date ? new Date(date) : "",
    //         seen: read,
    //         received: received,
    //         message: messageContent,
    //         [deletedBySenderID[0]]: true,
    //       };
    //     } else {
    //       messageData = {
    //         author: sender,
    //         created: date ? new Date(date) : "",
    //         seen: read,
    //         received: received,
    //         message: messageContent,
    //       };
    //     }
    //     console.log("writing");
    //     await chatRef.doc(messageID).collection("messages").add(messageData);
    //   });
    // });
  };
  const migrateRecommendations = () => {
    // var recommendations_threadRef = firestore.collection(
    //   "recommendation_threads"
    // );
    // var recommendationsRef = firestore.collection("recommendations");
    // const { recommendations } = data;
    // Object.keys(recommendations).map((uid) => {
    //   Object.keys(recommendations[uid]).map((types) => {
    //     Object.keys(recommendations[uid][types]).map(async (typeID) => {
    //       await recommendations_threadRef.doc(uid).set(
    //         {
    //           [types]: firebase.firestore.FieldValue.arrayUnion(typeID),
    //         },
    //         { merge: true }
    //       );
    //       await recommendationsRef
    //         .doc(typeID)
    //         .set(recommendations[uid][types][typeID]);
    //     });
    //   });
    // });
  };
  const migrateNotifications = () => {
    var notification_tokenRef = firestore.collection("notifications_token");
    var notificationRef = firestore.collection("notifications");

    const { notifications } = data;
    Object.keys(notifications).map((uid) => {
      Object.keys(notifications[uid]).map(async (notificationID) => {
        if (notificationID === "notificationTokens") {
          if (notifications[uid][notificationID]) {
            Object.keys(notifications[uid][notificationID]).map(
              async (tokenId) => {
                await notification_tokenRef.doc(uid).set(
                  {
                    Tokens: firebase.firestore.FieldValue.arrayUnion(tokenId),
                  },
                  { merge: true }
                );
              }
            );
          }
        } else {
          notificationRef
            .doc(uid)
            .collection("user_notifications")
            .add(notifications[uid][notificationID]);
        }
      });
    });
  };
  return (
    <>
      {" "}
      <MigrateBtn name="Migrate Users" onClick={migrateUsers}></MigrateBtn>
      <MigrateBtn
        name="Migrate Feeds/Posts"
        onClick={migrateFeeds}
      ></MigrateBtn>
      <MigrateBtn
        name="Migrate Comments"
        onClick={migrateComments}
      ></MigrateBtn>
      <MigrateBtn name="Migrate Likes" onClick={migrateLikes}></MigrateBtn>
      <MigrateBtn
        name="Migrate Messages"
        onClick={migrateMessages}
      ></MigrateBtn>
      <MigrateBtn
        name="Migrate Recommendations"
        onClick={migrateRecommendations}
      ></MigrateBtn>
      <MigrateBtn
        name="Migrate Notifications"
        onClick={migrateNotifications}
      ></MigrateBtn>
    </>
  );
};

export default Migration;
