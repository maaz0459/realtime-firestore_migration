import React from 'react'

export default function MigrateBtn({onClick,name}) {
    return (
        <div>
        <button style={{ marginTop: "20px" }} onClick={onClick}>
          {name}
        </button>
      </div>
    )
}
